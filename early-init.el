;;; early-init.el -*- lexical-binding: t; -*-
;; All of this cod is copied from doom's early-init file
(setq gc-cons-threshold most-positive-fixnum)

(setq package-enable-at-startup nil)
(unless (or (daemonp)
            noninteractive
            init-file-debug)
(setq-default inhibit-redisplay t
              inhibit-message t)
(add-hook 'window-setup-hook
          (lambda ()
            (setq-default inhibit-redisplay nil
                          inhibit-message nil)
            (redisplay)))

;; (define-advice load-file (:override (file) silence)
;;   (load file nil 'nomessage))
)


;;
;; (set-language-environment "UTF-8")

;; set-language-enviornment sets default-input-method, which is unwanted
;; (setq default-input-method nil)
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s seconds with %d garbage collections."
                     (emacs-init-time "%.2f")
                     gcs-done)))

