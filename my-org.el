;;; my-org-utils.el --- Org utilities that I use. -*- lexical-binding: t; -*-

(require 'org)
(require 'f)
;; fp extensions for the org api
(require 'dash)
(require 'org-ml)
;; (require 'gnus)

;; subr-x.el is a better string lib

;; (when (string-match "|\\([^|]*\\)|"  "|Wed,Thu| Some heading")  (substring "|Wed,Thu| Some heading" (match-end 0)))

;; Inspired by this function: clone-subtree-with-time-shift
;; Given n of weeks (positive number) and "date" string like:
;; |Mon,Wed,Thu| (stolen from org-recur)
;; make "n" copies of the subtree with the scheduled day (exactly like org-copy-subtree)
(defun tt/org-clone-subtree-weekly (n &optional week-str)
  ;; TODO make it so that a prefix with C-u or an optional arg
  ;; deletes the current subtree when you are done
  (interactive "nNumber of weeks to schedule: ")
  (when (org-before-first-heading-p)
    (user-error "No subtree to clone"))
  (let* (;; save the points to the beginning and end of subtree
	 (beg (save-excursion (org-back-to-heading t) (point)))
	 (end-of-tree
	  (save-excursion (org-end-of-subtree t t) (point)))
	 ;; stolen from org-recur
	 (org-weekly-regexp "|\\([^|]*\\)|")
	 (heading (org-get-heading t t t t))
	 ;; the pattern for getting regex from string is (when (string-match regex str)   (match-string 1 str))
	 (week-str
	  (or week-str
	      (when (string-match org-weekly-regexp heading)
		(match-string 1 heading))))
	 ;; heading without the matched expression (without leading or ending whitespace)
	 ;; TODO gnus-simplify-whitespace not work sometimes. string-clean-whitespace is a better function
	 (heading-new
	  (when (string-match org-weekly-regexp  heading)
	    (org-no-properties
	     (string-trim (substring heading (match-end 0))))))

	 (weekdays '("Sun" "Mon" "Tue" "Wed" "Thu" "Fri" "Sat"))
	 (weekday-p (lambda (str) (member str weekdays)))
	 ;; throw user-error if invalid week-str
	 (days
	  (cl-loop for str in
		   (split-string week-str ",")
		   collect
		   (let ;; Format day string to get rid of spaces and capitalize it to make it match the list
		       ((str (capitalize (string-trim str))))
		     (if (funcall weekday-p str)
			 str
		       (user-error (format "Not a weekday: %s" str)))))))
    (goto-char end-of-tree)
    ;; insert a newline if not at the beginning of the line
    (unless (bolp) (insert "\n"))
    ;; just insert the heading "n" times for now
    (let* ((end (point))
	   (nmax (1- n))
	   ;; save the subtree
	   (template (buffer-substring beg end))
	   ;; FIXME convert day to times (if today's a Monday, and
	   ;; "Mon" is in days) then this will schedule the "Mon" to
	   ;; the next available Monday So, you would also need to
	   ;; schedule the thing for the day or add some logic for
	   ;; that too..
	   ;; TO
	   (current-day
	    (nth
	     (string-to-number
	      (format-time-string "%w" (current-time)))
	     weekdays)))

      ;; Add n weeks to the day times
      ;; emacs-lisp loops include the last number
      (cl-loop for n from 1 to nmax do
	       ;; (insert "hello")
	       ;; (insert (with-temp-buffer  (insert " world")  (buffer-string)))
	       (insert
		(with-temp-buffer
		  ;; You need to call (org-mode) for org functions to work
		  (org-mode)
		  ;; FIXME If the current day is, say, a Mon day
		  ;; (org-read-date nil t ())
		  ;;

		  (cl-loop for day in days do
			   (let* (;; (format "+%s%s" n day) outputs a string like "+1Wed" for: https://orgmode.org/manual/The-date_002ftime-prompt.html
				  (s (format "+%s%s" n day))
				  (day-time (org-read-date nil t s)))
			     ;; TODO add the todo keyword to new headings. Also get rid of the match string in copies.
			     ;; (insert (format "%s\n" s))
			     (insert template)
			     ;; TODO Change here, if (current-day == day) then schedule to (n-1)
			     (org-schedule nil day-time)
			     (org-edit-headline heading-new)))
		  (buffer-string)))))))


;; this gets the property
;; (org-entry-get (point) "STYLE")

(defvar tt/org-agenda-projects-location 'nil "Default location of where to look for projects")
(setq tt/org-agenda-projects-location "~/code")



;; This checks if path s relative:
(defun tt/org-agenda-goto-project-or-research (arg)
  (interactive "P")
  (let ((marker
	 (or (org-get-at-bol 'org-hd-marker) (org-agenda-error))))
    (org-with-point-at marker
      (org-back-to-heading t)
      ;; (org-set-property "CATEGORY" "web")
      (let* ((maybe-project-file (org-entry-get (point) "PROJECT" nil))
	     (maybe-research-file
	      (org-entry-get (point) "RESEARCH" nil))
	     ;; FIXME not sure if there's a better pattern for this, but it works so..
	     (maybe-projects-root
	      (and tt/org-agenda-projects-location
		   (f-dir? tt/org-agenda-projects-location)
		   tt/org-agenda-projects-location)))

	;; 1. check if project file is nil. error if it is
	(when maybe-project-file
	  (if (f-relative? maybe-project-file)
	      (if maybe-projects-root
		  (find-file
		   (f-join maybe-projects-root maybe-project-file))
		(user-error "Projects root not set to a proper directory"))
	    (find-file maybe-project-file)))
	(when maybe-research-file
	  (if (file-exists-p maybe-research-file)
	      (find-file maybe-research-file)
	    (user-error "Research file does not exist!")))
	(user-error "The PROJECT or RESEARCH property is not set for the current heading")))))
;; rebinds p to my new function
(define-key org-agenda-mode-map
	    (kbd "p")
	    'tt/org-agenda-goto-project-or-research)


;; TODO not the best workflow for adding papers.. since org-ml just updates the subheadings..
;; I guess I could just compare subheadings - and only add ones that are changed.. this is later problem though..
(defvar tt/research-todo-dir "~/research-sync/papers/processor synthesis research" "directory that stores research papers for current project")
(defvar tt/research-todo-file "~/org/gtd/research.org" "file to store research todos")

(defun tt/org-ml-get-raw-value (node)
  (plist-get (car (cdr node)) :raw-value)
  )
;; get the end of a node
(defun tt/org-ml-get-end (node)
  (plist-get (car (cdr node)) :end)
  )
(defun tt/add-research-papers-to-todo ()
  (interactive)

  (let ((dirs
	 (directory-files
	  (expand-file-name tt/research-todo-dir)
	  nil
	  "^\\([^.]\\|\\.[^.]\\|\\.\\..\\)")))
    ;; https://orgmode.org/worg/dev/org-element-api.html
    ;; first - parse the org-gtd file
    ;; then - find the project heading that says has the same collection name - if not found create a new one
    ;; finally - add the files in the research dir to that collection as todo items
    ;; just trigger a capture template?
    (save-window-excursion
      ;; (switch-to-buffer-other-window
      ;;  (generate-new-buffer "*org-todo-window*"))
      (find-file tt/research-todo-file)
      (org-mode)
      ;; TODO grab the heading I want and add todo items
      ;; The point of org-ml is to make it easier to build / manipulate nodes.. but not whatever I'm doing?
      ;; (->> (org-ml-parse-this-subtree)
      ;; 	   (org-ml-headline-set-subheadlines (list (org-ml-build-headline! :level 2 :title-text "headline x")))
      ;; 	   (org-ml-to-trimmed-string))

      (let ((headline-match
	     (->>
	      (org-ml-parse-this-buffer)
	      (org-ml-match
	       '((:pred
		  (lambda (s)
		    (let ((str (plist-get (car (cdr s)) :raw-value)))
		      (string-match (f-base tt/research-todo-dir) str))))))
	      (car)
	      ;; (message "%s")
	      )))
	(let ((make-subheadline
	       (lambda (file)
		 (org-ml-build-headline! :level 2
					 :title-text (f-base file)
					 :todo-keyword "TODO"
					 :section-children (list
							    (org-ml-build-property-drawer!
							     (list 'RESEARCH
								   (intern
								    (concat tt/research-todo-dir "/" file))))
							    (org-ml-build-paragraph!
							     (org-ml-to-string
							      (org-ml-build-link
							       (concat tt/research-todo-dir "/" file)
							       :type "file"))))))))
	  (if headline-match
	      (let* ((old-dirs (->> (org-ml-headline-get-subheadlines headline-match)
				    (-map #'tt/org-ml-get-raw-value)
				    ;; gotta add back the extension
				    (--map (concat it ".pdf"))
				    ))
		     (new-dirs (cl-set-difference dirs old-dirs :test #'equal))
		     )
		;; (message "%s" new-dirs)
		(org-ml-insert
		 (tt/org-ml-get-end headline-match)
		 (-map make-subheadline new-dirs)
		 )
		;; (org-ml-update
		;;   (lambda (h)
		;;     (org-ml-headline-set-subheadlines
		;;      (-map make-subheadline dirs)
		;;      h))
		;;   headline-match)
		)
	      ;; insert a new heading
	      (org-ml-insert
	       (point-max)
	       (->>
		(org-ml-build-headline! :level 1
					:title-text (f-base tt/research-todo-dir)
					:tags (list "research")
					)
		(org-ml-headline-set-subheadlines
		 (-map make-subheadline dirs))))))

	)

      (unwind-protect (save-buffer) (kill-buffer) ;; (delete-window)
		      ))))




(provide 'my-org)
;; my-org.el ends here
